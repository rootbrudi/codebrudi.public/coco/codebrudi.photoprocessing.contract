namespace Codebrudi.PhotoProcessing.Contract.Exceptions;

[Serializable]
public class PhotoProcessingException : Exception
{
    public PhotoProcessingException()
    {
    }

    public PhotoProcessingException(string message) : base(message)
    {
    }

    public PhotoProcessingException(string message, Exception inner) : base(message, inner)
    {
    }
}
