namespace Codebrudi.PhotoProcessing.Contract.DataClasses;

public record Photo(string Name, byte[] Data);
