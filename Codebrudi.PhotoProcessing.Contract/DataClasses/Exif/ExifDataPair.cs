namespace Codebrudi.PhotoProcessing.Contract.DataClasses.Exif;

public record ExifDataPair(string Name, string Value);
