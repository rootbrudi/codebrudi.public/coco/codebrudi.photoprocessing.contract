using Codebrudi.PhotoProcessing.Contract.DataClasses;
using Codebrudi.PhotoProcessing.Contract.DataClasses.Exif;

namespace Codebrudi.PhotoProcessing.Contract.ExifManagement;

public interface IExifManager
{
    Task<ICollection<ExifDataPair>> GetExifDataAsync(Photo photo);
}
